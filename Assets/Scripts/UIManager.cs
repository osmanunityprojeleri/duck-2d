﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class UIManager : MonoBehaviour
{
    #region singleton

    private static UIManager _instance;
    public static UIManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<UIManager>();
            }
            return _instance;
        }
    }

    #endregion

    public UIObject[] panels; 
    void Start()
    {
        //Menü ile başlamak için.
        SetPanel(0); 
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetPanel(int activePanelId)
    {
        panels.ToList().ForEach(s => s.panel.SetActive(s.id == activePanelId));
    }

}

[Serializable]
public class UIObject
{
    public string name;
    public GameObject panel;
    public int id;
}