﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PlayerControlManager : MonoBehaviour
{
    #region singleton 
    private static PlayerControlManager _instance;
    public static PlayerControlManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<PlayerControlManager>();
            }
            return _instance;
        }
    }
    #endregion 
    public bool forceActive = false;
    public bool goLeftActive = false;
    public bool goRightActive = false;
    public float forceSpeed = 1.0f;
    public float leftRightSpeed = 0.4f;
    public float MinXPosition = -6.0f;
    public float MaxXPosition = 6.0f;
    public void AddForce(bool isActive)
    {
        forceActive = isActive;
    }

    public void GoRight(bool isActive)
    {
        goRightActive = isActive;
    }

    public void GoLeft(bool isActive)
    {
        goLeftActive = isActive;
    }

    private void FixedUpdate()
    {
        if (forceActive && transform.position.y < 4.1f)
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.up * forceSpeed);
            GameManager.Instance.fuel -= Time.deltaTime;
        }

        if (goRightActive && transform.position.x < MaxXPosition)
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.right * leftRightSpeed);
        }

        if (goLeftActive && transform.position.x > MinXPosition)
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.left * leftRightSpeed);
        }
    }
}
