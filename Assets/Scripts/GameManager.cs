﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    #region singleton

    private static GameManager _instance;
    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<GameManager>();
            }
            return _instance;
        }
    }

    #endregion

    public PlayerControlManager DuckControl;
    public GameObject Fuel;
    public float fuel = 100;
    public void StartGame()
    {
        fuel = 100;
    }

    void Start()
    {
        GameObject fuel = Instantiate(Fuel);
        
    }

    void Update()
    {
        //if (isPause)
        //{
        //    DuckControl.gameObject.GetComponent<Rigidbody2D>().Sleep();
        //}
        if (fuel < 0)
        {
            Debug.Log("Game Over - Fuel Finish");
        }
    }
}
