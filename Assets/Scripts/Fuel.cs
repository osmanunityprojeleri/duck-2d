﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fuel : MonoBehaviour
{
    public bool isActive = false;
    public Vector2 MaxPosition, MinPosition;
    public int fuelCount = 0;

    void Start()
    {
        GenerateFuel();
    }

    void GenerateFuel()
    {
        fuelCount = Random.Range(1, 10);
        transform.position = new Vector3(Random.Range(MinPosition.x, MaxPosition.x), Random.Range(MinPosition.y, MaxPosition.y), 0);
        gameObject.SetActive(true);
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            GameManager.Instance.fuel += fuelCount;

            if (GameManager.Instance.fuel > 100)
                GameManager.Instance.fuel = 100;
            gameObject.SetActive(false);
            Invoke("GenerateFuel", 2.0f);
        }
    }
}
